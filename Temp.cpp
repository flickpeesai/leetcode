#include <map>
#include <vector> 
#include <iostream> 

std::vector<int> LessThanEach(std::vector<int> & A, std::vector<int> & B)
{
    std::vector<int> Res; 
    if (!A.size() || !B.size()) return Res; 

    int vals[10000], sums[10000]; 
    std::fill(vals, vals + 10000, 0);
    std::fill(sums, sums + 10000, 0);

    for (auto a : A) 
        vals[a]++; 
    sums[0] = vals[0];
    for (int i=1; i<10000; i++) 
        sums[i] = vals[i] + sums[i-1]; 

    for (int j=0; j<B.size(); j++) 
        Res.push_back(sums[B[j]]); 
    return Res; 
}

void PrintVec(std::vector<int> V)
{
    if (!V.size()) return; 
    std::cout << "\nItems => "; 
    for (int i=0; i<V.size()-1; i++)
        std::cout << V[i] << ", ";
    std::cout << V.back() << "\n"; 
}


int SearchAtLeast(std::vector<int> & V, int l, int r, int target)
{
    while (l <= r) 
    {
        int m = l + (r-l)/2; 
        if (m < 0 || m > V.size()) break; 
        if (V[m] <  target) l = m+1; 
        if (V[m] >= target) r = m-1; 
    }
    return l; 
}

int SearchAtMost(std::vector<int> & V,  int l, int r, int target)
{
    while (l <= r) 
    {
        int m = l + (r-l)/2; 
        if (m < 0 || m > V.size()) break; 
        if (V[m] <= target) l = m+1; 
        if (V[m] >  target) r = m-1; 
    }
    return r; 
}

int CountPairs(std::vector<int> & V, int low, int upp)
{
    std::sort(V.begin(), V.end()); 
    int Res = 0; 
    for (int i=0; i<V.size(); i++)
    {
        int diffL = low-V[i]; 
        int diffU = upp-V[i]; 
        if (V[i] > upp) break; 
        
        int idxL = SearchAtLeast(V, i+1, V.size(), diffL), idxU = -1; 
        
        if (idxL >= 0 && idxL < V.size() && V[idxL] >= diffL && V[idxL] <= diffU) 
        {
            idxU = SearchAtMost(V,  i+1, V.size(), diffU); 
            if (idxU >= 0)
            {
                if (idxU < V.size())
                {
                    if (V[idxU] < diffU)
                    {
                        std::cout << "  " << V[i] << " = " << V[idxL] << "{" << idxL << "}" << " to " << V[idxU]  << "{" << idxU << "}" << "\n";
                        Res += (idxU-idxL+1);
                    }
                }
                else
                {
                    if (V.back() < diffU)
                    {
                        std::cout << "  " << V[i] << " = " << V[idxL] << "{" << idxL << "}" << " to " << V.back() << "{end}\n";
                        Res += (idxU-idxL);
                    }
                }
            }
        }
    }
    return Res; 
}

int main()
{
    std::cout << "How many pairs form sum inside range?\n\n"; 
    std::vector<int> v1 = {3,2,6}; 
    std::vector<int> v2 = {1,5}; 
    std::vector<int> v3 = {10,100,100,200,200}; 
    std::vector<int> v4 = {4,20,12,36,16,15,16,50}; 
    std::vector<int> v5 = {1,2,3}; 
    std::vector<int> v6 = {2,4};
    
    int s1 = CountPairs(v1, 7, 10); 
    std::cout << "For 7 => 10 ___ result = " << s1 << "\n\n"; 

    int s2 = CountPairs(v2, 10, 100); 
    std::cout << "For 10 => 100 ___ result = " << s2 << "\n\n"; 
    
    int s3 = CountPairs(v3, 300, 320); 
    std::cout << "For 300 => 320 ___ result = " << s3 << "\n\n"; 
    
    int s4 = CountPairs(v4, 30, 40); 
    std::cout << "For 30 => 40 ___ result = " << s4 << "\n\n"; 
    
    
    std::cout << "How many in A less than|equal to items in B?\n";
    
    PrintVec(LessThanEach(v1, v2)); 
    PrintVec(LessThanEach(v3, v4)); 
    PrintVec(LessThanEach(v5, v6)); 
}

