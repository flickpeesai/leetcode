#include <iostream> 
#include <vector> 
#include <queue> 


bool DFS(std::vector<std::vector<int>> & V, int i, int j, int m, int n) 
{
    if (i < 0 || j < 0 || i > m || j > n || V[i][j] <= 0) return false; 
    if (i == m && j == n) return true; 
    std::cout << i << "," << j << " "; 
    
    int vis  = V[i][j]; 
    V[i][j]  = -1; 
    bool nxt = DFS(V, i-1, j, m, n) || DFS(V, i, j-1, m, n) || DFS(V, i-1, j-1, m, n) || DFS(V, i-1, j+1, m, n) || 
               DFS(V, i+1, j, m, n) || DFS(V, i, j+1, m, n) || DFS(V, i+1, j-1, m, n) || DFS(V, i+1, j+1, m, n); 
    V[i][j]  = vis; 
    return   nxt; 
}


void BFS(std::vector<std::vector<int>> & V, int a, int b, int m, int n)
{
    std::queue<std::pair<int, int>> Q; 
    Q.push({a, b}); V[a][b] = 0; 
    
    while (!Q.empty()) 
    {
        auto [x,y] = Q.front(); Q.pop(); 
        std::vector<std::pair<int, int>> D = {{x,y+1}, {x,y-1}, {x-1,y}, {x+1,y}}; 
        for (auto & [nx,ny] : D) 
        {
            if (nx >= 0 && nx <= m && ny >= 0 && ny <= n && V[nx][ny] == 1) 
            {
                V[nx][ny] = 0; 
                Q.emplace(nx, ny); 
            }
        }
    }
}


int main()
{
    std::vector<std::vector<int>> V = 
    {
        {1, 1, 1, 1}, 
        {0, 0, 1, 0}, 
        {1, 1, 0, 0}, 
        {0, 1, 1, 1} 
    }; 
    int m = 4, n = 4; 
    
    //BFS(V, 1, 1, m-1, n-1); 
    for (auto v : V) 
    {
        for (auto e : v) std::cout << e << " "; 
        std::cout << "\n"; 
    }
    std::cout << "Can reach end? " << (DFS(V, 0, 0, m-1, n-1) ? "Yep." : "Nope.") << "\n"; 
}



