#include <iostream> 
#include <memory> 

struct List 
{
    int     data; 
    List *  next; 
}; 

struct LIST 
{
    int data; 
    std::shared_ptr<LIST> next; 
}; 


void Print(List * h) // Print entire list given List head 'h'. 
{
    while (h != nullptr) 
    {
        std::cout << h->data << " "; h = h->next; 
    }
    std::cout << "\n"; 
}

List * Merge(List * L1, List * L2) // Input lists sorted, can't have cycles or overlaps! 
{
    List dummy; List * tail = &dummy; 
    while (L1 != nullptr && L2 != nullptr) 
    {
        List *& node = (L1->data <= L2->data) ? L1 : L2; 
        tail  = tail->next = node; 
        node  = node->next; 
    }
    tail->next = L1 ? L1 : L2; 
    return dummy.next; 
}

List * Overlap(List * L1, List * L2) // Check if 2 lists have overlapping pointers. 
{
    List * A = L1, * B = L2; 
    while (A != B) 
    { 
        A = A ? A->next : L1; 
        B = B ? B->next : L2; 
    }
    return A; 
}

bool Cycles(List * head) // Check if list cycles on itself. 
{
    if (head == nullptr || head->next == nullptr) return false; 
    List * fast = head,  * slow = head; 
    while (fast->next && fast->next->next) 
    {
        fast = fast->next->next; 
        slow = slow->next; 
        if (fast == slow) return true; 
    }
    return false; 
}

List * Rev(List * head) // Reverse entire list. 
{
    if (head->next  == nullptr) return head; 
    List * p         = Rev(head->next); 
    head->next->next = head; 
    head->next       = nullptr; 
    
    std::cout << head->data << "_" << p->data << " "; 
    return p; 
}

List * Rev(List * head, int start, int end) // Reverse selected items in list. 
{
    if (start < 1 || end <= start) return head; 
    List *curr = head, *prev = nullptr; 
    int st = start, ed = end+1, k=1; 
    while (k++ < st) 
    {
        prev = curr; 
        curr = curr->next; 
    }
    
    List *conn = prev, *tail = curr, *temp = nullptr; 
    while (st++ < ed) 
    {
        temp = curr->next; 
        curr->next = prev; 
        prev = curr; 
        curr = temp; 
    }
    tail->next = curr; 

    if (conn != nullptr)    conn->next = prev; 
    else                    head = prev; 
    return head; 
}

bool Palindrome(List * head) // Check if list is a palindrome. 
{
    List * slow = head, * fast = head; 
    while (fast && fast->next) 
        fast = fast->next->next, slow = slow->next; 
    auto half1 = head, half2 = Rev(slow); 
    while (half1 && half2) 
    {
        if (half2->data != half1->data) return false; 
        half2 = half2->next, half1 = half1->next; 
    }
    return true; 
}
// List sorting => https://leetcode.com/problems/sort-list/ 


void Move(std::shared_ptr<LIST> * L, int n) // Traverse through list. 
{
    while (*L != nullptr && n--) 
        (*L) = (*L)->next; 
}

std::shared_ptr<LIST> Insert(std::shared_ptr<LIST> L, int n, int data) // Insert at position 'n'. 
{
    auto H = L; 
    if  (n < 1) return H; 
    Move(&L, n-1); 
    
    auto l  = std::make_shared<LIST>(); l->data = data; 
    auto t  = L->next; 
    l->next = t; 
    L->next = l; 
    return  H; 
}

std::shared_ptr<LIST> Remove(std::shared_ptr<LIST> L, int n) // Remove item at position 'n'. 
{
    auto H = L; 
    if  (n < 1) return H; 
    
    Move(&L, n-1); 
    if (L->next != nullptr && L->next->next != nullptr) 
        L->next  = L->next->next; 
    return H; 
}


int main()
{
    {
        List * p = new List, * h = p; p->data = 1; 
        for (int i=1; i<10; i++) 
        {
            List * t = new List; 
            t->data = i+1; 
            p->next = t; 
            p = p->next; 
        }
        h = Rev(h, 4, 8); 
        
        List * l1 = new List; l1->data = 4; 
        List * l2 = new List; l2->data = 8; 
        auto m = Merge(l1, l2); 
        
        Print(h); 
        Print(m); 
    }
    {
        auto L = std::make_shared<LIST>(); L->data = 1; 
        auto H = L; 
        for (int i=1; i<10; i++) 
        {
            L->next = std::make_shared<LIST>(); 
            L = L->next; 
            L->data = i+1; 
        }
    
        auto M = std::make_shared<LIST>(); M->data = 11; 
        auto I = M; 
        for (int i=11; i<20; i++) 
        {
            M->next = std::make_shared<LIST>(); 
            M = M->next; 
            M->data = i+1; 
        }
        
        Move(&H, 3); 
        L->next = I; 
        H = Insert(H, 8, 40); 
        H = Remove(H, 10); 
        
        while (H != nullptr) 
        {
            std::cout << H->data << " "; 
            H = H->next; 
        }
        std::cout << "\n"; 
    }
}

