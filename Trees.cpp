#include <iostream> 
#include <limits> 
#include <vector> 
#include <stack> 

struct Tree 
{
    int  data; 
    Tree *left, *right; 
}; 

struct BH { bool b; int  h; }; 
struct An { int  n; Tree * an; }; 


Tree * create(std::vector<int> & items, int l, int r) 
{
    int  m = (l+r)/2; 
    if (!items.size() || l >= r) return nullptr; 
    Tree * node = new Tree; 
	node->data  = items[m]; 
	node->left  = create(items, l,   m); 
	node->right = create(items, m+1, r); 
	return node; 
}

Tree * Reconstruct(std::vector<int> & V, int lo, int hi, int * r) 
{
    int root = V[*r], vs = V.size(); 
    if (*r == vs || root < lo || root > hi) return nullptr; 
    *r = *r+1; 
    auto left   = Reconstruct(V, lo, root, r); 
    auto right  = Reconstruct(V, root, hi, r); 
    Tree * node = new Tree; node->data = root; 
    node->left  = left; node->right  =  right; 
    return node; 
}

// Check if tree is a valid binary search tree. 
bool CheckBST(Tree * node, int lo, int hi) 
{
    if (node == nullptr) return true; 
    if (node->data < lo  || node->data > hi) return false; 
    return CheckBST(node->left, lo, node->data) && CheckBST(node->right, node->data, hi); 
}

BH balanced(Tree * node) 
{
    if (node  == nullptr)       return {true, -1}; 
    auto left  = balanced(node->left); 
    auto right = balanced(node->right); 
    if (!left.b  || !right.b)   return {false, 0}; 
    
    bool b = (std::abs(left.h-right.h) <= 1); 
    int  h = (std::max(left.h, right.h) + 1); 
    return {b, h}; 
}


void traverse(Tree * node, std::vector<int> & result) 
{
    if (node == nullptr) return; 
    if (node->left  != nullptr) std::cout <<  node->left->data << "<-"; 
    std::cout << node->data; 
    if (node->right != nullptr) std::cout << "->" << node->right->data; 
    std::cout << " "; 
    result.push_back(node->data); 
    traverse(node->left,  result); 
    traverse(node->right, result); 
}

void TraverseByLevel(std::vector<std::vector<int>> & Res, Tree * node, int lvl) 
{
    // Initialize with lvl = 0. 
    if (node == nullptr) return; 
    if (lvl  >= Res.size()) 
        Res.push_back({}); 
    Res[lvl].push_back(node->data); 
    TraverseByLevel(Res, node->left,  lvl+1); 
    TraverseByLevel(Res, node->right, lvl+1); 
}

std::vector<int> StackTraverse(Tree * root) 
{
    std::vector<int> Res; 
    std::stack<std::pair<Tree *, bool>> process; process.emplace(root, false); 
    while (!process.empty()) 
    {
        auto [node, left] = process.top(); process.pop(); 
        if (node != nullptr && left) 
        {
            Res.push_back(node->data); 
        }
        if (node != nullptr && !left) 
        {
            process.push({node->right,   false}); 
            process.push({node,           true}); 
            process.push({node->left,    false}); 
        }
    }
    return Res; 
}


Tree * search(Tree * node, int data) 
{
    if (node == nullptr)    return nullptr; 
    if (data >  node->data) return search(node->right, data); 
    if (data <  node->data) return search(node->left,  data); 
    if (data == node->data) return node; 
    return nullptr; 
}

An LCA(Tree * node, int n1, int n2) 
{
    // If BST, find n1 < node < n2. 
    // Keep going left if node > n2, keep going right if < n1. 
    if (node    == nullptr)     return {0,  nullptr}; 
    auto left   =               LCA(node->left,  n1, n2); 
    auto right  =               LCA(node->right, n1, n2); 
    if (left.n  == 2)           return left; 
    if (right.n == 2)           return right; 
    
    int n = left.n + right.n + (node->data == n1) + (node->data == n2); 
    return {n, (n == 2) ? node : nullptr}; 
}

void Klargest(Tree * node, int k, std::vector<int> & Res) 
{
    if (node != nullptr && Res.size() < k) 
    {
        Klargest(node->right, k, Res); 
        if (Res.size() < k) 
        {
            Res.push_back(node->data); 
            Klargest(node->left, k, Res); 
        }
    }
}


int main()
{
    std::vector<int> items; 
    for (int i=1; i<21; i++) 
        items.push_back(i); 
    Tree * tree = create(items, 0, items.size()); 
    Tree * head = tree; 
    
    Tree * node = search(tree, 3); 
    std::cout << "Search result: " << node << "\n"; 
    
    std::vector<int> Res, Rec, R, Lg; 
    std::vector<std::vector<int>> Vec = {{}}; 
    TraverseByLevel(Vec, head, 0); 
    traverse(head, Res); 
    R = StackTraverse(head); 
    std::cout << "\n"; 
    
    int r = 0; 
    Tree * recon =  Reconstruct(Res, std::numeric_limits<int>::min(), std::numeric_limits<int>::max(), &r); 
    traverse(recon, Rec); 
    std::cout << "\n"; 
    
    auto a = 1, b = 4; 
    auto bal = balanced(head); 
    auto lca = LCA(recon, a, b); 
    std::cout << "Tree balanced?  " << bal.b << "\n"; 
    std::cout << "Tree height =>  " << bal.h << "\n"; 
    std::cout << "LCA of " << a << ", " << b << " =>  " << lca.an->data << "\n"; 
    
    bool YN = CheckBST(head, std::numeric_limits<int>::min(), std::numeric_limits<int>::max()); 
    Klargest(head, 4, Lg); 
    std::cout << "Tree is valid BST? " << (YN ? "Yep." : "Nay.") << "\n"; 
    std::cout << "Largest 4 items :  "; 
    for (auto l : Lg) std::cout << l << " "; 
    std::cout << ".\n"; 
}

