CC		    = gcc
CXX		    = g++
RM		    = rm -f
CPPFLAGS	= -g
LDFLAGS		= 
LDLIBS		= -lmysqlcppconn

SRCS		= *.cpp 
OBJS		= $(subst .cc, .o, $(SRCS))

all: SQL

SQL: $(OBJS)
	$(CXX) $(LDFLAGS) -o SQL $(OBJS) $(LDLIBS) 

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) $(CPPFLAGS) -MM $^>>./.depend;

clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) *~ .depend

include .depend

