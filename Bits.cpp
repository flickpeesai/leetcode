#include <iostream>
#include <vector>
#include <string>
#include <bitset>

// Skipped: 
// - Multiplication, Division 
// - Integer palindromes 
// - Generate uniform random numbers 
// - Rectangle intersections 


uint32_t RevBits(uint32_t n) 
{
    n = (n >> 16) | (n << 16); 
    n = ((n & 0xff00ff00) >> 8) | ((n & 0x00ff00ff) << 8); 
    n = ((n & 0xf0f0f0f0) >> 4) | ((n & 0x0f0f0f0f) << 4); 
    n = ((n & 0xcccccccc) >> 2) | ((n & 0x33333333) << 2); 
    n = ((n & 0xaaaaaaaa) >> 1) | ((n & 0x55555555) << 1); 
    return n; 
}

long RevNums(int n) 
{
    long Res = 0; 
    while (n) 
    {
        Res = Res*10 + n%10; 
        n  /= 10; 
    }
    return Res; 
}

long Closest(long n) 
{
    for (int i=0; i<31; i++) 
    {
        if (((n>>i)&1) != ((n>>(i+1))&1)) 
        {
            n ^= (1<<i) | (1<<(i+1)); 
            return n; 
        }
    }
    return -1; // All bits 0s or 1s. 
}


int main() 
{
    // Int to binary string 
    std::string S = std::bitset<16>(1234).to_string(); 
    
    int x = 30, num = 0; 
    int i = 2,  j = 4; 
    
    // Count set bits 
    while (x > 0) 
    {
        num += x & 1; 
        x >>= 1; 
    }
    std::cout << "Set bits = " << num << "\n"; 
    
    // Erase lowest set bit 
    x  = 20; 
    x &= (x-1); 
    std::cout << "LSB erased = " << x << "\n"; 
    
    // Get parity of bits 
    x  = 4; 
    x ^= (x >> 16); 
    x ^= (x >>  8); 
    x ^= (x >>  4); 
    x ^= (x >>  2); 
    x ^= (x >>  1); 
    std::cout << "Parity of x = " << (x & 1) << "\n"; 
    
    // Get particular bits 
    x = 14; 
    std::cout << "The j-th bit = " << ((x>>j) & 1) << "\n"; 
    
    // Flip particular bits 
    x  = 18; 
    x ^= (1<<i) | (1<<j); 
    std::cout << "Bits " << i << " & " << j << " set = " << x << "\n"; 
}

