#include <functional> 
#include <algorithm> 
#include <iostream> 
#include <sstream> 
#include <vector> 
#include <string> 
#include <stack> 
#include <queue> 
#include <map> 

class SMax // Stack with max(). 
{
    struct CachedMax { int val, max; }; 
    std::stack<CachedMax> M; 
public: 
    void Push(int x) 
    { 
        int mx = std::max(x, M.empty() ? x : Max()); 
        M.emplace(CachedMax{x, mx}); 
    } 
    int  Pop() 
    {
        int item = M.top().val; M.pop(); 
        return item; 
    }
    int  Max() { return M.top().max; } 
}; 

class QMax // Queue with max(). 
{
    std::queue<int> Q; std::deque<int> Mx; 
public: 
    void Push(int x) 
    {
        Q.push(x); 
        while (!Mx.empty() && Mx.back() < x) 
            Mx.pop_back(); 
        Mx.push_back(x); 
    }
    int  Pop() 
    {
        int Res = Q.front(); Q.pop(); 
        if (Res == Mx.front()) 
            Mx.pop_front(); 
        return Res; 
    }
    int  Max() { return Mx.front(); } 
}; 

class VQueue // Queue using vector. 
{
    int k = 2, head = 0, tail = 0, num = 0; 
    std::vector<int> Q; 
public: 
    VQueue(int cap) : Q(cap) {} 
    void Push(int x) 
    { 
        if (num == Q.size()) 
        {
            std::rotate(Q.begin(), Q.begin()+head, Q.end()); 
            head = 0, tail = num; 
            Q.resize(Q.size()*k); 
        }
        Q[tail] = x; num++; 
        tail = (tail+1) % Q.size(); 
    } 
    int  Pop() 
    {
        int Res = Q[head]; num--; 
        head = (head+1) % Q.size(); 
        return Res; 
    }
}; 

class SQueue // Queue using 2 stacks. 
{
    std::stack<int> Q, DQ; 
public: 
    void Push(int x) 
    { 
        Q.emplace(x); 
    } 
    int  Pop() 
    {
        if (DQ.empty()) 
            while (!Q.empty()) 
                DQ.emplace(Q.top()), Q.pop(); 
        int Res = DQ.top(); DQ.pop(); 
        return Res; 
    }
}; 


const std::map<std::string, std::function<int(int, int)>> Ops = 
{
    {"+", [](int x, int y) -> int { return x+y; }}, 
    {"-", [](int x, int y) -> int { return x-y; }}, 
    {"*", [](int x, int y) -> int { return x*y; }}, 
    {"/", [](int x, int y) -> int { return x/y; }} 
}; 

const std::map<char, char> Look = 
{
    {'(',')'}, {'{','}'}, {'[',']'} 
}; 

int Eval(std::string Expr) // Evaulate RPN expressions. 
{
    std::stack<int> res; 
    std::stringstream ss(Expr); std::string tok; 
    while (std::getline(ss, tok, ',')) 
    {
        if (Ops.count(tok)) 
        {
            int y = res.top(); res.pop(); 
            int x = res.top(); res.pop(); 
            res.push(Ops.at(tok)(x, y)); 
        }
        else 
        {
            res.push(std::stoi(tok)); 
        }
    }
    return res.top(); 
}

bool Valid(std::string Expr) // Check if brackets are balanced. 
{
    std::stack<char> left; 
    for (int i=0; i<Expr.length(); i++) 
    {
        char c  = Expr[i]; 
        if (Look.count(c)) 
        {
            left.push(c); 
        }
        else 
        {
            if (left.empty() || Look.at(left.top()) != c) 
                return false; 
            left.pop(); 
        }
    }
    return left.empty(); 
}

struct BH { int i, h; }; 

std::vector<int> Sunset(std::vector<int>::iterator st, std::vector<int>::iterator ed) 
{
    std::vector<int> Res; 
    std::stack<BH>   Can; 
    int bid = 0; 
    while (st != ed) 
    {
        int h = *st++; 
        while (!Can.empty() && h >= Can.top().h) 
            Can.pop(); 
        Can.push(BH{bid++, h}); 
    }
    while (!Can.empty()) 
    {
        Res.push_back(Can.top().i); Can.pop(); 
    }
    return Res; 
}


int main()
{
    SMax sm; 
    sm.Push(1); std::cout << "SMax = " << sm.Max() << " ; "; 
    sm.Push(3); std::cout << "SMax = " << sm.Max() << " ; "; 
    sm.Push(2); std::cout << "SMax = " << sm.Max() << " \n"; 
    
    QMax qm; 
    qm.Push(1); std::cout << "QMax = " << qm.Max() << " ; "; 
    qm.Push(3); std::cout << "QMax = " << qm.Max() << " ; "; 
    qm.Push(2); std::cout << "QMax = " << qm.Max() << " \n"; 
    
    VQueue vq(2); 
    vq.Push(4); vq.Push(5); vq.Push(6); 
    std::cout << "VQueue Popped => " << vq.Pop() << " , " << vq.Pop() << " , " << vq.Pop() << "\n"; 
    
    SQueue sq; 
    sq.Push(4); sq.Push(5); sq.Push(6); 
    std::cout << "SQueue Popped => " << sq.Pop() << " , " << sq.Pop() << " , " << sq.Pop() << "\n"; 
    
    std::string Expr = "-641,6,/,28,/"; 
    std::cout << "Answer = "  << Eval(Expr)  << "\n"; 
    
    Expr = "{{((()))}}[][][][]"; 
    std::cout << "Balanced? " << Valid(Expr) << "\n"; 
    
    std::cout << "Buildings with view: "; 
    std::vector<int> B = {4,5,2,1,3}, Res = Sunset(B.begin(), B.end()); 
    for (auto r : Res) std::cout << B[r] << " ; "; 
    std::cout << "\n"; 
}
