#include <algorithm> 
#include <iostream> 
#include <vector> 
#include <string> 

void Print(std::vector<std::vector<int>> & V) 
{ 
    std::cout << "\n"; 
    for (auto v : V) 
    {
        for (auto i : v) std::cout << i << " "; 
        std::cout << " ;  "; 
    }
    std::cout << "\n"; 
}


void Choc(std::vector<int> & In, std::vector<int> T, std::vector<std::vector<int>> & V, int N, int R) 
{
    if (N <= 0 || R <= 0 || R > N) return; 
    if (R == 1) 
    {
        for (int i=N; i>0; i--) T[R-1] += (In[i-1]); 
        V.push_back(T); 
        return; 
    }
    for (int i=N; i>R-1; i--) 
    {
        T[R-1] += (In[i-1]); 
        Choc(In, T, V, i-1, R-1); 
    }
}

void LNS(std::vector<int> & V, std::vector<int> & A) 
{
    if (!A.size()) return; 
    for (int i=1; i<A.size(); i++) 
        for (int j=0; j<i; j++) 
            if (A[i] >= A[j]) V[i] = std::max(V[i], V[j]+1); 
}


void Hanoi(int pieces, char A, char B, char C) 
{
    // Eg. Hanoi(4, 'A', 'B', 'C') for 4 levels in the tower. 
    if (pieces <= 0) return; 
    if (pieces == 1) 
    {
        std::cout << "Moving piece 1 from " << A << " to " << C << ".\n"; 
        return; 
    }
    Hanoi(pieces-1, A, C, B); 
    std::cout << "Moving piece " << pieces << " from " << A << " to " << C << ".\n"; 
    Hanoi(pieces-1, B, A, C); 
}

void Brackets(std::vector<std::string> & V, std::string S, int L, int R) 
{
    if (L == 0 && R == 0) 
    {
        V.push_back(S); 
        return; 
    }
    if (L > 0) Brackets(V, S+"< ", L-1, R); 
    if (R > L) Brackets(V, S+"> ", L, R-1); 
}

bool Is(std::string & S) 
{
    for (int i=0, j=S.length()-1; i<j; i++, j--) 
        if (S[i] != S[j]) 
            return false; 
    return true; 
}
void Pal(std::vector<std::string> & V, std::string & T, int off) 
{
    if (off >= T.length()-1) return; 
    
    for (int i=off+1; i<T.length(); i++) 
    {
        std::string t = T.substr(off, i-off); 
        if (Is(t) && t.length() > 1) 
            if (std::find(V.begin(), V.end(), t) == V.end()) 
                V.push_back(t); 
        Pal(V, T, i); 
    }
}


int main()
{
    std::vector<int> In = {1,2,3,4,5}; 
    std::vector<std::vector<int>> V; 
    {
        int R = 2; 
        std::vector<int> T(R, 0); 
        Choc(In, T, V, In.size(), R); 
        Print(V); V.clear(); 
    }
    {
        int R = 3; 
        std::vector<int> T(R, 0); 
        Choc(In, T, V, In.size(), R); 
        Print(V); V.clear(); 
    }
    {
        std::vector<std::string> Res; 
        Brackets(Res, "", 3, 3); 
    
        std::string T = "02028825678760"; 
        Pal(Res, T, 0); 
        for (auto v : Res) std::cout << v << "\n"; 
    }
}





