#include <algorithm> 
#include <iostream> 
#include <vector> 
#include <string> 
#include <queue> 


struct Intv { int l, r; }; // Assumes l < r. 

void Custom(std::vector<Intv> & V) 
{
    std::sort(V.begin(), V.end(), [](const Intv & a, const Intv & b) 
    {
        if (a.l == b.l) return (a.r <= b.r); 
        return (a.l < b.l); 
    }); 
}

std::vector<int> Intersect(std::vector<int> & A, std::vector<int> & B) // A, B sorted. 
{
    std::vector<int> Res; 
    int i=0, j=0; 
    while (i < A.size() && j < B.size()) 
    {
        if (A[i] == B[j] && (i == 0 || A[i] != A[i-1])) 
        {
            Res.push_back(A[i]); 
            i++; j++; 
        }
        else if (A[i] < B[j]) i++; 
        else if (A[i] > B[j]) j++; 
    }
    return Res; 
}

void Merge(std::vector<int> & A, std::vector<int> & B, int m, int n) // A, B sorted; A has extra space for B. 
{
    int a = m-1, b = n-1,       i = m+n-1; 
    while (a >= 0 && b >= 0)    A[i--] = (A[a] > B[b]) ? A[a--] : B[b--]; 
    while (b >= 0)              A[i--] = B[b--]; 
}

int HIndex(std::vector<int> & A) // A sorted; Largest h => h entries >= h. 
{
    int n = A.size(); 
    for (int i=0; i<n; i++) 
        if (A[i] >= n-i) return (n-i); 
    return 0; 
}

int CantMake(std::vector<int> & A) // A sorted; Eg. [1,2,5] cant make 4. 
{
    int Res = 0; 
    for (auto a : A) 
    {
        if (a > Res+1) break; 
        Res += a; 
    }
    return Res+1; 
}

double SalaryCap(double T, std::vector<int> & S) // S sorted; Cap items in S s.t. Sum(S) = T. 
{
    double sum = 0.0; 
    for (int i=0; i<S.size(); i++) 
    {
        int people = S.size() - i; 
        double adj = S[i] * people; 
        if (sum + adj >= T) 
            return (T-sum)/people; 
        sum += S[i]; 
    }
    return -1.0; 
}


int Bsearch(std::vector<int> & V, int T) 
{
    int l = 0, r = V.size()-1; 
    while (l <= r) 
    {
        int m = l+((r-l)/2); 
        if (V[m]  < T) l = m+1; 
        if (V[m] == T) return m; 
        if (V[m]  > T) r = m-1; 
    }
    return -1; 
}

int Leftmost(std::vector<int> & V, int T) 
{
    int l = 0, r = V.size()-1, Res = -1; 
    while (l <= r) 
    {
        int m = l+(r-l)/2; 
        if (V[m]  < T) l = m+1; 
        if (V[m] == T) Res = m, r = m-1; 
        if (V[m]  > T) r = m-1; 
    }
    return Res; 
}

// WARNING: Doesn't always work if target has duplicates! 
int Magic(std::vector<int> & V) 
{
    int l = 0, r = V.size()-1; 
    while (l <= r) 
    {
        int m = l+((r-l)/2); 
        if (V[m]  < m) l = m+1; 
        if (V[m] == m) return m; 
        if (V[m]  > m) r = m-1; 
    }
    return -1; 
}

double SquareRoot(unsigned int k) // Assumes k>1; FP error < 0.1%. 
{
    double l = 0, r = k, m = l+(0.5*(r-l)), err = 0.001; 
    while (std::abs(m*m-k) > err) 
    {
        if (m*m < k) l = m; 
        if (m*m > k) r = m; 
        m = l+(0.5*(r-l)); 
    }
    return m; 
}

std::pair<int, int> Search2D(std::vector<std::vector<int>> & V, int T) 
{
    if (!V.size() || !V[0].size()) return {-1,-1}; 
    int r = 0, c = V[0].size()-1; 
    while (r < V.size() && c >= 0) 
    {
        if (V[r][c] == T) return {r+1,c+1}; 
        if (V[r][c]  < T) r++; 
        if (V[r][c]  > T) c--; 
    }
    return {-1,-1}; 
}

int KthLargest(std::vector<int> & V, int k) // Remove std::greater for k-th smallest. 
{
    if (!V.size() || k >= V.size()) return -1; 
    std::priority_queue<int, std::vector<int>, std::greater<int>> Q; 
    for (auto v : V) 
    {
        Q.push(v); 
        if (Q.size() > k) Q.pop(); 
    }
    return Q.top(); 
}

std::vector<double> OnlineMedian(std::vector<double>::iterator st, std::vector<double>::iterator ed) 
{
    std::vector<double> Res; 
    std::priority_queue<int, std::vector<int>, std::less<int>>    MaxH; 
    std::priority_queue<int, std::vector<int>, std::greater<int>> MinH; 
    while (st != ed) 
    {
        MinH.emplace(*st++); 
        MaxH.emplace(MinH.top()); MinH.pop(); 
        if (MaxH.size() > MinH.size()) 
            MinH.emplace(MaxH.top()), MaxH.pop(); 
        double med = 0.5*(MinH.top()+MaxH.top()); 
        Res.push_back(MinH.size() == MinH.size() ? med : MinH.top()); 
    }
    return Res; 
}


int main()
{
    {
        std::vector<int>  A = {1,2,5}; 
        std::cout << "Can't make $" << CantMake(A) << " with coins.\n"; 
        
        std::vector<Intv> V = {{3,5},{1,4},{3,4},{2,5}}; 
        Custom(V); 
        for (auto v : V) std::cout << v.l << "," << v.r << " "; 
        std::cout << "\n"; 
        
        std::vector<int>  S = {20,30,40,90,100}; 
        std::cout << "Salary cap = $" << SalaryCap(210, S) << " .\n"; 
    }
    {
        std::vector<int> A = {-4,1,4,5,6}; 
        std::cout << "Binary search => " << Magic(A) << "\n"; 
        
        std::vector<std::vector<int>> V = {{1,2,3,4}, {5,6,7,8}, {9,10,11,12}, {13,14,15,16}}; 
        auto Res = Search2D(V, 12); 
        std::cout << "Binary search => " << Res.first << "," << Res.second << "\n"; 
        
        std::vector<double> L = {1,2,3,4,5,6,7,8}, Med = OnlineMedian(L.begin(), L.end()); 
        std::cout << "Online median values => "; 
        for (auto m : Med) std::cout << m << " ; "; 
        std::cout << "\n"; 
    }
}
