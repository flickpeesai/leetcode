#include <iostream> 
#include <random> 
#include <vector> 
#include <string> 
#include <cmath> 


void EvenOdd(std::vector<int> & A) 
{
    int i = 0, j = A.size()-1; 
    while (i < j) 
    {
        if (A[i] % 2 == 0)  i++; 
        else                std::swap(A[i], A[j--]); 
    }
}

void ThreeParts(std::vector<int> & A, int idx) 
{
    int pv = A[idx]; 
    int sm = 0, eq = 0, lg = A.size(); 
    while (eq < lg) 
    {
        if (A[eq] < pv)         std::swap(A[sm++], A[eq++]); 
        else if (A[eq] == pv)   eq++; 
        else                    std::swap(A[eq], A[--lg]); 
    }
}

// Skipped: increment Int stored as Vec<int>. 
std::vector<int> Multiply(std::vector<int> & N1, std::vector<int> & N2) 
{
    int sign = (N1.front()<0) ^ (N2.front()<0) ? -1 : 1; 
    N1.front() = std::abs(N1.front()), N2.front() = std::abs(N2.front()); 
    
    std::vector<int> Res(N1.size()+N2.size(), 0); 
    for (int i=N1.size()-1; i>=0; i--) 
    {
        for (int j=N2.size()-1; j>=0; j--) 
        {
            Res[i+j+1]  += N1[i] * N2[j]; 
            Res[i+j]    += Res[i+j+1]/10; 
            Res[i+j+1]  %= 10; 
        }
    }
    Res.front() *= sign; 
    return Res; 
}

// Skipped: Delete dups from sorted array. 
bool ReachEnd(std::vector<int> & A) 
{
    int far = 0, last = A.size()-1; 
    for (int i=0; i<=far && far<last; i++) 
        far = std::max(far, A[i]+i); 
    return far >= last; 
}


double StockOnce(std::vector<double> & A) 
{
    double Min = 10000, Max = 0; // Better to use numeric limits. 
    for (auto a : A) 
    {
        double today = a-Min; 
        Max =  std::max(Max, today); 
        Min =  std::min(Min, a); 
    }
    return Max; 
}

double StockTwice(std::vector<double> & A) 
{
    double Min = 10000, Max = -10000, Total = 0; // Better to use numeric limits. 
    std::vector<double> first(A.size(), 0); 
    for (int i=0; i<A.size(); i++) 
    {
        Min      = std::min(Min,   A[i]); 
        Total    = std::max(Total, A[i]-Min); 
        first[i] = Total; 
    }
    for (int i=A.size()-1; i>0; i--) 
    {
        Max      = std::max(Max,   A[i]); 
        Total    = std::max(Total, Max-A[i]+first[i-1]); 
    }
    return Total; 
}

void Alternate(std::vector<int> & A) // A < B > C < D and so on. 
{
    for (int i=1; i<A.size(); i++) 
        if ((!(i%2) && A[i-1] < A[i]) || ((i%2) && A[i-1] > A[i])) 
            std::swap(A[i], A[i-1]); 
}


// Another option: std::next_permutation(S.begin(), S.end()); 
// Also available: https://en.cppreference.com/w/cpp/algorithm/prev_permutation 

void Next(std::vector<int> & Curr) 
{
    auto invert =  std::is_sorted_until(Curr.rbegin(), Curr.rend()); 
    if  (invert == Curr.rend()) return; 
    auto least  =  std::upper_bound(Curr.rbegin(), invert, *invert); 
    std::iter_swap(least,       invert); 
    std::reverse(Curr.rbegin(), invert); 
}

std::vector<int> SamOn(std::vector<int>::iterator St, std::vector<int>::iterator Ed, int k) 
{
    std::random_device R; std::default_random_engine Ran(R()); 
    std::vector<int> Res; 
    for (int i=0; i<k; i++) Res.push_back(*St++); 
    int num = k; 
    while (St != Ed) 
    {
        int x = *St++; num++; 
        std::uniform_int_distribution<int> Seed(0, num-1); 
        if (int i = Seed(Ran); i < k) Res[i] = x; 
    }
    return Res; 
}

void Clockwise(std::vector<std::vector<int>> & A, int off, std::vector<int> & Res) 
{
    int as = A.size()-1; 
    if (off == as-off) 
    {
        Res.push_back(A[off][off]); 
        return; 
    }
    for (int j=off; j<as-off; j++)  Res.push_back(A[off][j]); 
    for (int i=off; i<as-off; i++)  Res.push_back(A[i][as-off]); 
    for (int j=as-off; j>off; j--)  Res.push_back(A[as-off][j]); 
    for (int i=as-off; i>off; i--)  Res.push_back(A[i][off]); 
}
std::vector<int> Spiral(std::vector<std::vector<int>> & A) 
{
    std::vector<int> Res; 
    for (int off=0; off<std::ceil(0.5*A.size()); off++) 
        Clockwise(A, off, Res); 
    return Res; 
}


int main()
{
    std::vector<double> V = {12,11,13,9,12,8,14,13,15}; 
    std::cout << "Max profit = " << StockTwice(V) << "\n"; 
    
    std::vector<int> A = {1,2,3,4,5,6}; 
    Alternate(A); 
    for (auto a : A) std::cout << a << " "; 
    std::cout << "\n"; 
    
    std::vector<int> X = {4,3,5,7,3,5,6,3,5,7,9,5,3,2,5,8}; 
    std::vector<int> Y = SamOn(X.begin(), X.end(), 10); 
    for (auto y : Y) std::cout << y << " "; 
    std::cout << "\n"; 
}


// Sliding window hash table => https://leetcode.com/problems/minimum-window-substring/ 



